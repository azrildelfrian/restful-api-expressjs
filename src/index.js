const express = require("express");
const dotenv = require("dotenv");
const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();
const app = express();

//konfigurasi library dotenv agar bisa mengambil data dari file .env
dotenv.config();

//mendefinisikan port dari file .env
const PORT = process.env.PORT;

//digunakan untuk mem-parse
app.use(express.json());

//Pengetesan server
app.get("/api", (req, res) => {
  res.send("HELLO WORLD");
});

//Fungsi GET untuk mengambil semua data
app.get("/buku", async (req, res) => {
  const buku = await prisma.buku.findMany();

  res.send(buku);
});

//fungsi POST untuk menambah data
app.post("/buku", async (req, res) => {
  const newBuku = req.body;

  const buku = await prisma.buku.create({
    data: {
      judul: newBuku.judul,
      pengarang: newBuku.pengarang,
      tahun: newBuku.tahun,
      penerbit: newBuku.penerbit,
    },
  });

  res.send({
    data: buku,
    messager: "berhasil tambah",
  });
});

//Fungsi DELETE untuk menghapus data
app.delete("/buku/:id", async (req, res) => {
  const bukuId = req.params.id;

  await prisma.buku.delete({
    where: {
      id: parseInt(bukuId),
    },
  });
  res.send("Dihapus");
});

//Fungsi PUT untuk update semua fields
app.put("/buku/:id", async (req, res) => {
  const bukuId = req.params.id;
  const bukuData = req.body;
  const buku = await prisma.buku.update({
    where: {
      id: parseInt(bukuId),
    },
    data: {
      judul: bukuData.judul,
      pengarang: bukuData.pengarang,
      tahun: bukuData.tahun,
      penerbit: bukuData.penerbit,
    },
  });
  res.send({
    data: buku,
    messager: "berhasil edit",
  });
});

//Menjalankan server dengan port yang telah ditentukan
app.listen(PORT, () => {
  console.log(`API is running on port: ` + PORT);
});
